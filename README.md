# Vagrant Environment for Unit Testing Workshop

A vagrant environment for the unit testing workshop. You will need Vagrant and
VirtualBox installed on your machine.

You will need to install the latest version of Vagrant onto your machine, otherwise
it is likely that during the VM boot process Vagrant will exit.

If you are unable to get a Vagrant environment running, a PHP 7 & MySQL setup on your host machine will suffice.

## Running the VM

Add to your hosts file (/etc/hosts on OSX or Linix)

`10.11.100.233 unit-testing.local`

Download the repository

`git clone https://bitbucket.org/mkpeacock/unit-testing-workshop.git unit-testing-workshop`

Move into the project folder

`cd unit-testing-workshop`

Initialise the submodules

`git submodule init`

Update the submodules

`git submodule update`

Boot the VM

`vagrant up`
