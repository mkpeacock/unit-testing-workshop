class php () {

    package { "php-fpm":
      ensure => present,
    }

    service { "php7.0-fpm":
        require => Package['php-fpm']
    }

    package { "php-cli":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-curl":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-gd":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-json":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-mcrypt":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-xml":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-mbstring":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-soap":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-xdebug":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

    package { "php-zip":
        ensure => "present",
        notify => Service['php7.0-fpm']
    }

}
