group { 'puppet': ensure => 'present' }

host { 'unit-testing.local':
    ip => '127.0.0.1',
    host_aliases => ['unit-testing', 'localhost'],
}

$databases = {
  'workshop' => {
    ensure  => 'present',
    charset => 'utf8'
  },
  'workshop_testing' => {
    ensure  => 'present',
    charset => 'utf8'
  },
}

$users = {
  'workshop@localhost' => {
    ensure                   => 'present',
    max_connections_per_hour => '0',
    max_queries_per_hour     => '0',
    max_updates_per_hour     => '0',
    max_user_connections     => '0',
    password_hash            => '*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19',
  },
  'workshop_testing@localhost' => {
    ensure                   => 'present',
    max_connections_per_hour => '0',
    max_queries_per_hour     => '0',
    max_updates_per_hour     => '0',
    max_user_connections     => '0',
    password_hash            => '*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19',
  },
}

$grants = {
  'workshop@localhost/workshop.*' => {
    ensure     => 'present',
    options    => ['GRANT'],
    privileges => ['ALL'],
    table      => 'workshop.*',
    user       => 'workshop@localhost',
  },
  'workshop_testing@localhost/workshop_testing.*' => {
    ensure     => 'present',
    options    => ['GRANT'],
    privileges => ['ALL'],
    table      => 'workshop_testing.*',
    user       => 'workshop_testing@localhost',
  },
}

class { '::mysql::server':
  root_password    => 'rootpassword',
  override_options => { 'mysqld' => { 'max_connections' => '1024' } },
  databases => $databases,
  users => $users,
  grants => $grants,
  restart => true
}

include '::mysql::client'

class { '::mysql::bindings':
  php_enable => true,
  php_package_name => 'php-mysql'
}

include 'beanstalkd'

class {
    'nginx':
        file => 'default',
}


class {
    'php':
}

class wget { package{ 'wget': ensure => present } }
class gcc { package{ 'gcc': ensure => present } }

package { "curl":
    ensure => present
}

package { "git-core":
    ensure => present
}

package { "postfix":
    ensure => present
}

package { "mailutils":
    ensure => present
}

package { "ntp":
    ensure => present
}

#class { 'timezone':
#  region   => 'Europe',
#  locality => 'London',
#}
