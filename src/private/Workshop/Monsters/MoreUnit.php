<?php

namespace Workshop\Monsters;

class MoreUnit
{
    public function formatStrictLicenseKey($key)
    {
        $key = preg_replace("/[^A-Za-z0-9]/", '', $key);

        if (strlen($key) != 16) {
            throw new \LengthException();
        }

        return chunk_split($key, 4, '-');
    }

    public function extractValidEmailAddresses(array $emailAddresses): array
    {
        $validEmailAddresses = [];

        foreach ($emailAddresses as $emailAddress) {
            if (false !== filter_var($emailAddress, \FILTER_VALIDATE_EMAIL)) {
                $validEmailAddresses[] = $emailAddress;
            }
        }
    }

    public function removeStopWords(string $phrase, array $stopWords = []): array
    {
        $wordsRemoved = [];
        $phrase = explode(' ', $phrase);

        foreach ($phrase as $word) {
            $clean = trim(preg_replace("/[^A-Za-z0-9]/", '', $key));

            if (!in_array($clean, $stopWords)) {
                $wordsRemoved[] = $word;
            }
        }

        return implode(' ', $wordsRemoved);
    }
}
