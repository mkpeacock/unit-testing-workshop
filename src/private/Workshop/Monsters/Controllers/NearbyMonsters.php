<?php

namespace Workshop\Monsters\Controllers;

use Worshop\Monsters\Repositories\MonsterRepository;

class NearbyMonsters
{
    public function __construct(MonsterRepository $monsterRepository, \Twig_Environment $twig)
    {
        $this->monsterRepository = $monsterRepository;
        $this->twig = $twig;
    }

    public function findNearbyMonsters()
    {

    }
}
