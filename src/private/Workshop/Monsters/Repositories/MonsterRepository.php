<?php

namespace Workshop\Monsters\Repositories;

use Workshop\Monsters\Models\Monster;
use Workshop\Monsters\Exceptions\ModelNotFound;

class MonsterRepository implements Repository
{
    protected $pdo;
    protected $repositoryFactory;

    public function __construct(\Pdo $pdo, RepositoryFactory $repositoryFactory)
    {
        $this->pdo = $pdo;
        $this->repositoryFactory = $repositoryFactory;
    }

    public function newInstance(array $properties = [])
    {
        $monster = new Monster(
            $this->pdo,
            $this->repositoryFactory->getUserRepository(),
            $this->repositoryFactory->getHidingPlaceRepository()
        );
        $monster->hydrate($properties);

        return $monster;
    }

    public function findNearby($latitude, $longitude)
    {
        $radius = 10;

        $sql = "SELECT m.*,
                       (
                            3959 * acos(
                                cos(
                                    radians(:latitude)
                                ) * cos(
                                    radians(h.latitude)
                                ) * cos(
                                    radians(h.longitude) - radians(:longitude)
                                ) + sin(
                                    radians(:latitude)
                                ) * sin(
                                    radians(h.latitude)
                                )
                            )
                        ) AS distance
                    FROM
                        monsters m,
                        hiding_monsters h
                    WHERE
                        m.id = h.monster_id
                    HAVING
                        distance < :radius
                    ORDER BY
                        distance
                    ";

            $statement = $this->pdo->prepare($sql);
            $statement->bindValue(':latitude', $latitude, \PDO::PARAM_STR);
            $statement->bindValue(':longitude', $longitude, \PDO::PARAM_STR);
            $statement->bindValue(':radius', $radius, \PDO::PARAM_INT);

            $statement->execute();

            return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getFromPrimaryKey($primaryKeyValue)
    {
        $sql = "SELECT * FROM monsters WHERE id = :id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindValue(':id', $primaryKeyValue, \PDO::PARAM_INT);
        $statement->execute();

        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) != 1) {
            throw new ModelNotFound();
        }

        return $this->newInstance($data[0]);
    }
}
