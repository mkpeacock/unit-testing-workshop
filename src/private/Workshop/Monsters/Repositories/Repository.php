<?php

namespace Workshop\Monsters\Repositories;

interface Repository
{
    public function newInstance(array $properties = []);

    public function getFromPrimaryKey($primaryKeyValue);
}
