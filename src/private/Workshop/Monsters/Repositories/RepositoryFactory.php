<?php

namespace Workshop\Monsters\Repositories;

class RepositoryFactory
{
    public function __construct(\Pdo $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getUserRepository()
    {
        return new UserRepository($this->pdo, $this);
    }

    public function getMonsterRepository()
    {
        return new MonsterRepository($this->pdo, $this);
    }

    public function getHidingPlaceRepository()
    {
        return new HidingPlaceRepository($this->pdo, $this);
    }
}
