<?php

namespace Workshop\Monsters\Repositories;

use Workshop\Monsters\Models\HidingPlace;
use Workshop\Monsters\Models\Monster;
use Workshop\Monsters\Exceptions\ModelNotFound;

class HidingPlaceRepository implements Repository
{
    protected $pdo;
    protected $repositoryFactory;

    public function __construct(\Pdo $pdo, RepositoryFactory $repositoryFactory)
    {
        $this->pdo = $pdo;
        $this->repositoryFactory = $repositoryFactory;
    }

    public function newInstance(array $properties = [])
    {
        $hidingPlace = new HidingPlace($pdo, $this->$repositoryFactory->getMonsterRepository());
        $hidingPlace->hydrate($properties);

        return $hidingPlace;
    }

    public function getHidingPlacesForMonster(Monster $monster)
    {
        $sql = "SELECT * FROM hiding_monsters WHERE monster_id = :monster_id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindValue(':monster_id', $monster->getId(), \PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function getFromPrimaryKey($primaryKeyValue)
    {
        $sql = "SELECT * FROM hiding_monsters WHERE id = :id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindValue(':id', $primaryKeyValue, \PDO::PARAM_INT);
        $statement->execute();

        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) != 1) {
            throw new ModelNotFound();
        }

        return $this->newInstance($data[0]);
    }
}
