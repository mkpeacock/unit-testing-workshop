<?php

namespace Workshop\Monsters\Repositories;

use Workshop\Monsters\Models\User;

class UserRepository implements Repository
{
    protected $pdo;
    protected $repositoryFactory;

    public function __construct(\Pdo $pdo, RepositoryFactory $repositoryFactory)
    {
        $this->pdo = $pdo;
        $this->repositoryFactory = $repositoryFactory;
    }

    public function newInstance(array $properties = [])
    {
        $user = new User($this->pdo, $this->repositoryFactory->getMonsterRepository());
        $user->hydrate($properties);

        return $user;
    }

    public function getFromPrimaryKey($primaryKeyValue)
    {
        $sql = "SELECT * FROM users WHERE id = :id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindValue(':id', $primaryKeyValue, \PDO::PARAM_INT);
        $statement->execute();

        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) != 1) {
            throw new ModelNotFound();
        }

        return $this->newInstance($data[0]);
    }
}
