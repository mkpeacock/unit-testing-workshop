<?php

namespace Workshop\Monsters\Models;

use Workshop\Monsters\Repositories\MonsterRepository;
use Workshop\Monsters\Exceptions\MonsterNotCatchable;

class User implements Saveable, Owner
{
    use Hydratable;

    protected $id;
    protected $name;
    protected $email;
    protected $password;

    public function __construct(\Pdo $pdo, MonsterRepository $monsterRepository)
    {
        $this->pdo = $pdo;
        $this->monsterRepository = $monsterRepository;
    }

    public function isPassword($password)
    {
        return password_verify($password, $this->password);
    }

    public function capture(Capturable $capturable)
    {
        try {
            $monster->setOwner($capturable);
        } catch (\MonsterNotCatchable $e) {
            throw $e;
        }
    }

    // Althought this is provided for us magically, we have an interface
    // that requires the method exist explicitally
    public function getId()
    {
        return $this->id;
    }

    public function save()
    {
        // Persist to database
    }
}
