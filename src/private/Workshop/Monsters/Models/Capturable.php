<?php

namespace Workshop\Monsters\Models;

interface Capturable
{
    public function setOwner(Owner $owner);
}
