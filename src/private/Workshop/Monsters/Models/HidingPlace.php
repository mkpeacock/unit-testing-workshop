<?php

namespace Workshop\Monsters\Models;

use Workshop\Monsters\Models\Monster;
use Workshop\Monsters\Repositories\MonsterRepository;

class HidingPlace
{
    protected $id;
    protected $monsterId;
    protected $latitude;
    protected $longitude;

    protected $monster = null;

    use Hydratable;

    public function __construct(\Pdo $pdo, MonsterRepository $monsterRepository)
    {
        $this->pdo = $pdo;
        $this->monsterRepository = $monsterRepository;
    }

    public function getMonster()
    {

    }

    public function setMonster(Monster $monster)
    {
        $this->monster = $monster;
        $this->monsterId = $monster->getId();
    }

    public function delete()
    {

    }

}
