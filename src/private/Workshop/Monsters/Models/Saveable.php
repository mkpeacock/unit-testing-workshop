<?php

namespace Workshop\Monsters\Models;

interface Saveable
{
    public function save();
}
