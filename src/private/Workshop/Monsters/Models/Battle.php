<?php

namespace Workshop\Monsters\Models;

class Battle
{
    protected $attacker;
    protected $defender;
    protected $turn = 'attacker';
    protected $winner = null;
    protected $turns = [];

    public function __construct(Monster $attacker, Monster $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }

    public function attackWith(Attack $attack)
    {
        $this->defender->attack($attack);
        $this->endTurn();
    }

    public function retaliateWith(Attack $attack)
    {
        $this->attacker->attack($attack);
        $this->endTurn();
    }

    public function endTurn()
    {
        $this->turn = ($this->turn == 'attacker') ? 'defender' : 'attacker';
    }

    public function getMonsterWhoseTurnItIs()
    {
        if ($this->turn == 'attacker') {
            return $this->attacker;
        }

        return $this->defender;
    }

    public function getMonsterWhoseTurnItIsNext()
    {
        if ($this->turn == 'defender') {
            return $this->attacker;
        }

        return $this->defender;
    }

    public function getWinner()
    {
        if (is_null($this->winner)) {
            throw new \Exception('There is no winner yet');
        }

        return $this->winner;
    }
}
