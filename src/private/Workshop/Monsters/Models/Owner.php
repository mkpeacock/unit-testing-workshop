<?php

namespace Workshop\Monsters\Models;

interface Owner
{
    public function getId();
}
