<?php

namespace Workshop\Monsters\Models;

class Turn
{
    protected $defenderHealthBeforeTurn;
    protected $attacker;
    protected $defender;
    protected $attack;
    protected $damageInflicted;

    public function __construct(Monster $attacker, Monster $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;

        $this->defenderHealthBeforeTurn = $defender->getHealth();
    }

    public function withMove(Attack $attack)
    {
        $this->attack = $attack;
        // How much damage can the attacker do?
        $this->damageInflicted = $this->attacker->getDamageAbility($attack);

        if ($this->damageInflicted > $this->defenderHealthBeforeTurn) {
            $this->defender->setHealth(0);
        } else {
            $this->defender->setHealth($this->defender->getHealth() - $this->damageInflicted);
        }
    }

    public function isWinningTurn()
    {
        return ($this->damageInflicted > $this->defenderHealthBeforeTurn);
    }
}
