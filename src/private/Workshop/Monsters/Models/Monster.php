<?php

namespace Workshop\Monsters\Models;

use Workshop\Monsters\Exceptions\MonsterNotCatchable;
use Workshop\Monsters\Exceptions\MonsterOwnedByYou;

use Workshop\Monsters\Repositories\HidingPlaceRepository;
use Workshop\Monsters\Repositories\UserRepository;

class Monster implements Saveable, Capturable
{
    protected $id;
    protected $name;
    protected $ownerId;
    protected $health;

    protected $owner;
    protected $pdo;
    protected $userRepository;
    protected $hidingPlaceRepository;

    use Hydratable;

    public function __construct(\Pdo $pdo, UserRepository $userRepository, HidingPlaceRepository $hidingPlaceRepository)
    {
        $this->pdo = $pdo;
        $this->userRepository = $userRepository;
        $this->hidingPlaceRepository = $hidingPlaceRepository;
    }

    public function getOwner()
    {
        if (is_null($this->owner) && !is_null($this->ownerId)) {
            $this->owner = $this->userRepository->getFromPrimaryKey($this->ownerId);
        }

        return $this->owner;
    }

    public function setOwner(Owner $owner)
    {
        if ($this->isOwned()) {
            if ($this->isOwnedBy($owner)) {
                throw new MonsterOwnedByYou();
            }

            throw new MonsterNotCatchable();
        }

        if (!$monster->isHidden()) {
            throw new MonsterNotCatchable();
        }

        if (!$monster->isDefeated()) {
            throw new MonsterNotCatchable();
        }

        $this->owner = $owner;
        $this->ownerId = $owner->getId();
        $this->save();
    }

    public function isOwned()
    {
        return ! is_null($this->ownerId);
    }

    public function isOwnedBy(Owner $user)
    {
        return ($this->ownerId == $user->getId());
    }

    public function isHiding()
    {
        $hidingPlaces = $this->hidingPlaceRepository->getHidingPlacesForMonster($this);

        return (count($hidingPlaces) > 0);
    }

    public function isDefeated()
    {
        return ($this->health <= 0);
    }

    public function save()
    {
        // Persist to database
        echo 'Saving monster';
    }
}
