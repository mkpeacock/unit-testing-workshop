<?php

namespace Workshop\Monsters\Models;

trait Hydratable
{
    public function hydrate(array $properties)
    {
        foreach ($properties as $databaseField => $value) {
            $this->$databaseField = $value;
        }
    }

    public function __get($name)
    {
        $name = str_replace('_', ' ', $name);
        $name = ucwords($name);
        $name = str_replace(' ', '', $name);
        $getter = 'get' . $name;

        return $this->$getter();
    }

    public function __set($name, $value)
    {
        // Call setPropertyName so that if we want to prevent
        // access to the property, or do something special with the input
        // we can
        $name = str_replace('_', ' ', $name);
        $setter = 'set' . (str_replace(' ', '', ucwords($name)));

        return $this->$setter($value);
    }

    public function __call($name, $arguments)
    {
        if (strpos($name, 'set') === 0 && strlen($name) > 3) {
            $property = $this->methodNameToProperty($name);
            $this->$property = $arguments[0];

            return $this;
        } elseif (strpos($name, 'get') === 0 && strlen($name) > 3) {
            $property = $this->methodNameToProperty($name);

            return isset($this->$property) ? $this->$property : null;
        }
    }

    protected function methodNameToProperty($method_name)
    {
        return lcfirst(substr($method_name, 3, strlen($method_name)));
    }
}
