<?php

namespace Workshop\Monsters\Cages;

use Workshop\Monsters\Models\Monster;

class CageFactory
{
    public function forBreedName(string $breedName)
    {
        switch ($breedName) {
            case 'fire':
                return new FireResistantCage();
            case 'water':
                return new RustResistantCage();
            default:
                return new Cage();
        }
    }

    public function forBreed()
    {

    }

    public function forMonster(Monster $monster)
    {

    }
}
