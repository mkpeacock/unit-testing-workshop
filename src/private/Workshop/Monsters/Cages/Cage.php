<?php

namespace Workshop\Monsters\Cages;

class Cage
{
    protected $monster = null;

    public function __construct(Monster $monster = null)
    {
        $this->monster = $monster;
    }

    public function isEmpty()
    {
        return is_null($this->monster);
    }

    public function getMonster()
    {
        return $this->monster;
    }

    public function removeMonster()
    {
        $monster = $this->monster;
        $this->monster = null;

        return $monster;
    }
}
