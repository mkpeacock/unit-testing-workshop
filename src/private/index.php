<?php

date_default_timezone_set('UTC');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/bootstrap.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();

$app['debug'] = true;

$app['pdo'] = function () {
    $dsn = 'mysql:dbname=workshop;host=127.0.0.1';
    $user = 'workshop';
    $password = 'password';

    return new \Pdo($dsn, $user, $password);
};

$app['repository_factory'] = function ($app) {
    return new \Workshop\Monsters\Repositories\RepositoryFactory($app['pdo']);
};

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__. '/templates',
));

$app->get('/users/register', function(Request $request) use($app) {
    return 'Hello ';
});

$app->post('/users/register', function(Request $request) use($app) {
    return 'Hello ';
});

$app->get('/', function(Request $request) use($app) {

    $mf = $app['repository_factory']->getMonsterRepository();

    $monster = $mf->getFromPrimaryKey(1);
    echo $monster->getName();

    return new Response(
        $app['twig']->render('index.twig')
    );
});

$app->run();
