<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Untestable code

$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];

$dsn = 'mysql:dbname=workshop;host=127.0.0.1';
$user = 'workshop';
$password = 'password';


$pdo = new \Pdo($dsn, $user, $password);
$sql = "SELECT m.*,
               (
                    3959 * acos(
                        cos(
                            radians(:latitude)
                        ) * cos(
                            radians(h.latitude)
                        ) * cos(
                            radians(h.longitude) - radians(:longitude)
                        ) + sin(
                            radians(:latitude)
                        ) * sin(
                            radians(h.latitude)
                        )
                    )
                ) AS distance
            FROM
                monsters m,
                hiding_monsters h
            WHERE
                m.id = h.monster_id
            HAVING
                distance < :radius
            ORDER BY
                distance
            ";
$statement = $pdo->prepare($sql);
$statement->bindValue(':latitude', $latitude, \PDO::PARAM_STR);
$statement->bindValue(':longitude', $longitude, \PDO::PARAM_STR);
$statement->bindValue(':radius', 10, \PDO::PARAM_INT);
$statement->execute();
$result = $statement->fetchAll(\PDO::FETCH_ASSOC);

echo '<pre>' . print_r($result, true) . '</pre>';
